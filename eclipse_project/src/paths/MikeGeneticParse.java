
package paths;

import java.util.function.Function;
import paths.Path2d.Direction;
import paths.coordinates.Coordinate2d;

public class MikeGeneticParse {
    
    
    public static Coordinate2d nextNode(Coordinate2d start, Direction d){
        Coordinate2d retCoord = new Coordinate2d(start.x, start.y);
        switch(d){
            case UP:
                return new Coordinate2d(start.x, start.y -1);
            case DOWN:
                return new Coordinate2d(start.x, start.y + 1);
            case LEFT:
                return new Coordinate2d(start.x - 1, start.y);
            case RIGHT:
                return new Coordinate2d(start.x + 1, start.y);
            default:
                return new Coordinate2d(start.x, start.y);
        }
    }
    
    public static Coordinate2d compileDirections(Coordinate2d startNode, Path2d.Direction[] directions){
        Coordinate2d endNode = startNode;
        for(Path2d.Direction d : directions){
            endNode = nextNode(endNode, d);
        }
        return endNode;
    }
    
}
