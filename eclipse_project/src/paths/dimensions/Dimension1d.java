package paths.dimensions;

import paths.coordinates.Coordinate1d;

public class Dimension1d extends Dimension<Coordinate1d> {

    public final int l;

    public Dimension1d(int l) {
        super(l);
        this.l = l;
    }

}
