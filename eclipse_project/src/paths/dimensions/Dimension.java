package paths.dimensions;

import java.util.ArrayList;
import paths.coordinates.Coordinate;

public abstract class Dimension<COORDTYPE extends Coordinate> {

    protected final int[] allDims;

    protected Dimension(int... allDims) {
        this.allDims = allDims;
    }

    public final int[] dimensions() {
        return allDims;
    }

    public final int size() {
        return allDims.length;
    }

    public static void justifyDimensions(Dimension dim, ArrayList searchSpace) {
        int forAllMult = 1;
        for (int d : dim.dimensions()) {
            forAllMult *= d;
        }
        if (forAllMult != searchSpace.size()) {
            throw new IllegalArgumentException("Illegal dimension size for searchspace.");
        }
    }

}
