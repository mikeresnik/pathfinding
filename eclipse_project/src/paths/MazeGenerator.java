package paths;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.function.Function;

import org.apache.log4j.Logger;

import com.anji.integration.Activator;
import com.anji.neat.Evolver;

import paths.coordinates.Coordinate2d;
import paths.neat.MazeTargetFitnessFunction;
import paths.neat.NeatPath2d;
import paths.neat.NeatUtil;
import util.images;
import util.maps;

public final class MazeGenerator {

	private static final String DATA_LOCATION = "data/";
	private static final String MAZE_LOCATION = DATA_LOCATION + "mazes/";
	private static final int NUM_ELEMENTS = 20;
	private static final Function<Coordinate2d, String> TABLE_FORMAT = new Function<Coordinate2d, String>() {
		@Override
		public String apply(Coordinate2d t) {
			return t.x + "\t" + t.y + "\n";
		}
	};
	private static final Function<Coordinate2d, String> COORD_FORMAT = new Function<Coordinate2d, String>() {
		@Override
		public String apply(Coordinate2d t) {
			return "(" + t.x + ", " + t.y + ") ";
		}
	};

	private static class Cell {

		public int x, y;
		public List<WALL> walls = new ArrayList<WALL>(Arrays.asList(WALL.values()));

		public enum WALL {
			UP(new int[] { 0, -1 }), DOWN(new int[] { 0, 1 }), LEFT(new int[] { -1, 0 }), RIGHT(new int[] { 1, 0 });
			public int[] direction;

			private WALL(int[] direction) {
				this.direction = direction;
			}

			public static WALL getOpposite(WALL input) {
				switch (input) {
				case UP:
					return DOWN;
				case DOWN:
					return UP;
				case LEFT:
					return RIGHT;
				case RIGHT:
					return LEFT;
				}
				return null;
			}
		}

		public Cell(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public WALL randWall() {
			if (walls.isEmpty()) {
				return null;
			}
			final int randIndex = (int) (Math.random() * walls.size());
			WALL retWall = walls.get(randIndex);
			walls.remove(randIndex);
			return retWall;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final Cell other = (Cell) obj;
			if (this.x != other.x) {
				return false;
			}
			if (this.y != other.y) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			return "Cell{" + "x=" + x + ", y=" + y + ", walls=" + walls + '}';
		}

	}

	private MazeGenerator() {

	}

	private static final Map<Integer, Character> DEFAULT_MAP = new HashMap() {
		{
			this.put(-1, 'C');
			this.put(-16777216, 'W');
		}
	};
	private static final Map<Character, Integer> INVERSE_MAP = maps.invertUniqueMap(DEFAULT_MAP);

	public static char[][] recursiveBacktracker(int passed) {
		final int MAZE_SIZE = passed + 2;
		Cell initial = new Cell((int) (Math.random() * MAZE_SIZE), (int) (Math.random() * MAZE_SIZE));
		// Randomly pick from remaining walls as directions
		Stack<Cell> visited = new Stack();
		List<Cell> traversed = new ArrayList();
		visited.push(initial);
		traversed.add(initial);
		Cell current = initial;
		while (visited.isEmpty() == false) {
			Cell.WALL wall = current.randWall();
			if (wall == null) {
				// make current node the popped node and continue
				current = visited.pop();
				continue;
			}
			// if null or visited, make current node the popped node and recycle
			int[] direction = wall.direction;
			int[] newLoc = new int[] { current.x + direction[0], current.y + direction[1] };
			Cell newCell = new Cell(newLoc[0], newLoc[1]);
			if (traversed.contains(newCell)) {
				current = visited.pop();
				continue;
			}
			if (newLoc[0] >= MAZE_SIZE || newLoc[0] < 0 || newLoc[1] >= MAZE_SIZE || newLoc[1] < 0) {
				// if direction gives out of bounds, recycle
				continue;
			}
			newCell.walls.remove(Cell.WALL.getOpposite(wall));
			current = newCell;
			visited.add(newCell);
			traversed.add(newCell);
			// else make the current node the new direction node and recycle
		}
		char[][] representation = new char[2 * MAZE_SIZE + 1][2 * MAZE_SIZE + 1];
		for (int i = 0; i < 2 * MAZE_SIZE + 1; i++) {
			for (int j = 0; j < 2 * MAZE_SIZE + 1; j++) {
				representation[i][j] = 'C';
			}
		}
		for (Cell curr : traversed) {
			// System.out.println(curr);
			final int row = 2 * curr.y, col = 2 * curr.x;
			// System.out.println(curr.walls);
			for (Cell.WALL curr_wall : curr.walls) {
				// System.out.println(".");
				int[] dir = curr_wall.direction;
				// System.out.println("dir:" + Arrays.toString(dir));
				int[] oldLoc = new int[] { col, row };
				int[] newLoc = new int[] { Math.max(col + dir[0], 0), Math.max(row + dir[1], 0) };
				if (Arrays.equals(oldLoc, newLoc)) {
					// System.out.println("CONTINUE");
					continue;
				}
				// System.out.println(Arrays.toString(newLoc));
				representation[newLoc[1]][newLoc[0]] = 'W';
			}
		}
		for (int i = 0; i < MAZE_SIZE; i++) {
			representation[0][i] = 'W';
			representation[MAZE_SIZE - 1][i] = 'W';
			representation[i][0] = 'W';
			representation[i][MAZE_SIZE - 1] = 'W';
		}
		for (int i = 0; i < 2 * passed + 1; i += 2) {
			for (int j = 0; j < 2 * passed + 1; j += 2) {
				representation[i][j] = 'W';
			}
		}
		char[][] retArray = new char[MAZE_SIZE][MAZE_SIZE];
		for (int i = 0; i < MAZE_SIZE; i++) {
			for (int j = 0; j < MAZE_SIZE; j++) {
				retArray[i][j] = representation[i][j];
			}
		}
		for (int i = 0; i < MAZE_SIZE; i++) {
			for (int j = 0; j < MAZE_SIZE; j++) {
				if (retArray[i][j] == 'C') {
					Coordinate2d currentCoordinate = new Coordinate2d(j, i);
					List<Character> neighborValues = currentCoordinate
							.getNeighborValues(Util.charToCharacter(retArray));
					boolean allWalls = true;
					for (Character curr : neighborValues) {
						if (curr == null) {
							continue;
						}
						if (curr == 'C') {
							allWalls = false;
						}
					}
					if (allWalls) {
						retArray[j][i] = 'W';
					}
				}
			}

		}
		return retArray;

	}

	public static boolean isMazeCompletable(char[][] inputMaze) {
		// use DFS to find if is completable
		Character[][] maze_objective = Util.charToCharacter(inputMaze);
		Coordinate2d start = Util.findFirst(maze_objective, 'C');
		Coordinate2d end = Util.findLast(maze_objective, 'C');
		DFS2d path = new DFS2d(start, end, inputMaze);
		path.isActive = true;
		while (path.isActive) {
			try {
				path.step();
			} catch (MazeException me) {
				return false;
			}
		}
		return true;
	}

	public static void generateMazeToFile(int passed) {
		Save.toBMP(recursiveBacktracker(passed), MAZE_LOCATION + "maze" + passed + ".bmp");
	}

	public static final class Load {

		private Load() {

		}

		private static char[][] fromBMPSpecific(String pathName, Map<Integer, Character> loadMap) {
			int[][] pixels = null;
			try {
				pixels = images.loadImageToIntArray(pathName, BufferedImage.TYPE_INT_RGB);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			int height = pixels.length;
			int width = pixels[0].length;
			char[][] retMaze = new char[height][width];
			for (int ROW_INDEX = 0; ROW_INDEX < height; ROW_INDEX++) {
				for (int COL_INDEX = 0; COL_INDEX < width; COL_INDEX++) {
					retMaze[ROW_INDEX][COL_INDEX] = loadMap.getOrDefault(pixels[ROW_INDEX][COL_INDEX], 'C');
				}
			}
			return retMaze;
		}

		public static char[][] fromBMP(String pathName) {
			return fromBMPSpecific(pathName, MazeGenerator.DEFAULT_MAP);
		}

		public static Queue<char[][]> multiple(int size) {
			String fileLocation = MazeGenerator.MAZE_LOCATION + size + "/";
			File file = new File(fileLocation);
			if (file.exists() == false) {
				System.out.println("FILE DOES NOT EXIST. GENERATING MAZES . . . ");
				Save.multiple(size, MazeGenerator.NUM_ELEMENTS);
			}
			Queue<char[][]> retQueue = new ArrayDeque();
			for (int i = 0; i < MazeGenerator.NUM_ELEMENTS; i++) {
				retQueue.add(fromBMP(fileLocation + i + ".bmp"));
			}
			return retQueue;
		}

	}

	public static final class Score {

		private Score() {

		}

		public static void scoreAllFormat(int size, String fileTag, Function<Coordinate2d, String> toString) {
			// loadall
			// score all path finding algorithms in different files.
			String fileLoc = MazeGenerator.MAZE_LOCATION + size + "/";
			Queue<char[][]> mazes = MazeGenerator.Load.multiple(size);

			scoreBased(fileLoc, mazes, new DFS2d(null, null, new Character[][] {}), "dfs" + fileTag, toString);
			System.out.println("DFS Completed");
			scoreBased(fileLoc, mazes, new BFS2d(null, null, mazes.peek()), "bfs" + fileTag, toString);
			System.out.println("BFS Completed");
			scoreBased(fileLoc, mazes, new AStar2d(null, null, mazes.peek()), "astar" + fileTag, toString);
			System.out.println("AStar Completed");
			// neat2d
			neatPath2d(fileLoc, mazes, "neat" + fileTag, toString);
			/**
			 * train on all mazes, pick best chromosome per maze to write to a file. list of
			 * stringlists
			 */

		}

		public static <K extends Path2d> void scoreBased(String fileLoc, Queue<char[][]> mazes, K input_path,
				String nameTag, Function<Coordinate2d, String> toString) {
			String pathFileLoc = fileLoc + nameTag + ".txt";
			File pathFile = new File(pathFileLoc);
			PrintWriter pw = null;
			try {
				pw = new PrintWriter(pathFile);
			} catch (FileNotFoundException fnfe) {
				fnfe.printStackTrace();
			}
			for (char[][] maze : mazes) {
				Character[][] mazeCharacter = MazeGenerator.Util.charToCharacter(maze);
				Coordinate2d start = MazeGenerator.Util.findFirst(mazeCharacter, 'C'),
						end = MazeGenerator.Util.findLast(mazeCharacter, 'C');
				// DFS2d path = new DFS2d(start, end, maze);
				K path = (K) input_path.generate(start, end, MazeGenerator.Util.charToCharacter(maze));
				Coordinate2d[] pruned = path.backtrackMaze();
				String recent = "";
				for (Coordinate2d curr : pruned) {
					recent = toString.apply(curr);
					pw.print(recent);
				}
				if (recent.contains("\n") == false) {
					pw.println();
				}
			}
			pw.close();
		}

		public static void scoreAllCoordFormat(int size, boolean... normalize) {
			scoreAllFormat(size, "_coord", COORD_FORMAT);
		}

		public static void scoreAllTableFormat(int size) {
			scoreAllFormat(size, "_table", TABLE_FORMAT);
		}
		
		private static void neatPath2d(String fileLoc,Queue<char[][]> mazes,  String nameTag, Function<Coordinate2d, String> toString) {
			String pathFileLoc = fileLoc + nameTag + ".txt";
			String propertiesFile = "properties/maze_test.properties";
			File pathFile = new File(pathFileLoc);
			PrintWriter pw = null;
			try {
				pw = new PrintWriter(pathFile);
			} catch (FileNotFoundException fnfe) {
				fnfe.printStackTrace();
			}
			for(char[][] maze : mazes) {
				Character[][] mazeCharacter = MazeGenerator.Util.charToCharacter(maze);
				Coordinate2d start = MazeGenerator.Util.findFirst(mazeCharacter, 'C'),
						end = MazeGenerator.Util.findLast(mazeCharacter, 'C');
				MazeTargetFitnessFunction.setVals(start, end, mazeCharacter);
				Logger logger = Logger.getLogger( Evolver.class );
				NeatUtil.evolveProperties(propertiesFile);
				Activator activator = NeatUtil.getActivatorMax();
				NeatPath2d path = new NeatPath2d(start, end, mazeCharacter, activator);
				path.calculateFitness();
				List<Coordinate2d> visited = path.getVisitedRepresentation();
				String recent = "";
				for (Coordinate2d curr : visited) {
					recent = toString.apply(curr);
					pw.print(recent);
				}
				if (recent.contains("\n") == false) {
					pw.println();
				}
			}
			pw.close();
		}
		
	}

	public static void printMaze(char[][] mazeInput) {
		int i = 0;
		int jSize = mazeInput[0].length;
		int[] top = new int[jSize];
		for (int j = 0; j < jSize; j++) {
			top[j] = j;
		}
		System.out.println("\t" + Arrays.toString(top));
//        for (int j = mazeInput.length - 1; j >= 0; j--) {
//            char[] ROW = mazeInput[j];
//            System.out.println(i + "\t" + Arrays.toString(ROW));
//            i++;
//        }

		for (char[] ROW : mazeInput) {
			System.out.println(i + "\t" + Arrays.toString(ROW));
			i++;
		}
	}

	public static final class Save {

		private Save() {

		}

		public static void toBMP(char[][] inputMaze, String pathName) {
			toBMPSpecific(inputMaze, pathName, MazeGenerator.INVERSE_MAP);
		}

		private static void toBMPSpecific(char[][] inputMaze, String pathName, Map<Character, Integer> saveMap) {
			int[][] converted = new int[inputMaze.length][inputMaze[0].length];
			for (int ROW_INDEX = 0; ROW_INDEX < inputMaze.length; ROW_INDEX++) {
				for (int COL_INDEX = 0; COL_INDEX < inputMaze[0].length; COL_INDEX++) {
					converted[ROW_INDEX][COL_INDEX] = saveMap.getOrDefault(inputMaze[ROW_INDEX][COL_INDEX], -1);
				}
			}
			try {
				images.saveImageFromIntArray(converted, pathName, BufferedImage.TYPE_INT_RGB);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}

		public static void multiple(int size, int amount) {
			String fileLocation = MAZE_LOCATION + size + "/";
			File file = new File(fileLocation);
			file.mkdirs();
			for (int index = 0; index < amount; index++) {
				char[][] currMaze = MazeGenerator.recursiveBacktracker(size);
				while (MazeGenerator.isMazeCompletable(currMaze) == false) {
					currMaze = MazeGenerator.recursiveBacktracker(size);
				}
				toBMP(currMaze, fileLocation + index + ".bmp");
			}
		}

	}

	public static final class Util {

		public static Character[][] charToCharacter(char[][] input) {
			Character[][] retArray = new Character[input.length][input[0].length];
			for (int i = 0; i < input.length; i++) {
				for (int j = 0; j < input[0].length; j++) {
					retArray[i][j] = input[i][j];
				}
			}
			return retArray;
		}

		public static <K> Coordinate2d findLast(K[][] searchSpace, K matching) {
			for (int i = searchSpace.length - 1; i >= 0; i--) {
				for (int j = searchSpace[i].length - 1; j >= 0; j--) {
					if (matching.equals(searchSpace[i][j])) {
						return new Coordinate2d(j, i);
					}
				}
			}
			return null;
		}

		public static <K> Coordinate2d findFirst(K[][] searchSpace, K matching) {
			for (int i = 0; i < searchSpace.length; i++) {
				for (int j = 0; j < searchSpace[i].length; j++) {
					if (matching.equals(searchSpace[i][j])) {
						return new Coordinate2d(j, i);
					}
				}
			}
			return null;
		}

		public static List<String> applyToString(List<Coordinate2d> coords, Function<Coordinate2d, String> toString){
			List<String> retList = new ArrayList();
			for(Coordinate2d curr : coords) {
				retList.add(toString.apply(curr));
			}			
			return retList;
		}
		
		private Util() {

		}

	}

	public static void main(String[] args) {
		// char[][] testMaze = Load.fromBMP("testMaze.bmp");
		// printMaze(testMaze);
		// Save.toBMP(testMaze, "testMazeCOPY.bmp");
		// Save.toBMP(recursiveBacktracker(21), "maze21.bmp");
		// Save.multiple(15, NUM_ELEMENTS);
		// Main core runner of this program.
		Queue<char[][]> mazes = Load.multiple(100);

		DFS2d depth;
		Queue<List<Coordinate2d>> traversalShit = new ArrayDeque();

		for (char[][] maze : mazes) {
			Character[][] objMaze = Util.charToCharacter(maze);
			depth = new DFS2d(Util.findFirst(objMaze, 'C'), Util.findLast(objMaze, 'C'), maze);

			depth.isActive = true;
			while (depth.isActive) {
				depth.step();
			}
			traversalShit.add(depth.getVisitedRepresentation());
			System.out.println("Solved one, length " + depth.getVisitedRepresentation().size());
		}
		float avg = 0.0f;
		for (List<Coordinate2d> path : traversalShit) {
			avg += path.size();
		}
		avg /= traversalShit.size();
		System.out.println("your chromosome should be " + avg + " long fucko for a size 100 maze");
	}

}
