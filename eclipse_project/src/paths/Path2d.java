package paths;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import static paths.Path2d.Direction.*;
import paths.coordinates.Coordinate2d;

public abstract class Path2d<CELL> extends Path<Coordinate2d, CELL> {

    public final CELL[][] searchSpace;
    public Integer[][] traversalSpace;
    public Direction direction;

    public Path2d(Coordinate2d start, Coordinate2d end, CELL[][] searchSpace, CELL wallRepresentation) {
        super(start, end, wallRepresentation);
        this.searchSpace = searchSpace;
        this.traversalSpace = ( searchSpace.length != 0 ? new Integer[searchSpace.length][searchSpace[0].length] : new Integer[][]{}) ;
        final Integer defInt = null;
        for (int ROW = 0; ROW < traversalSpace.length; ROW++) {
            for (int COL = 0; COL < traversalSpace[0].length; COL++) {
                traversalSpace[ROW][COL] = defInt;
            }
        }
        this.setSteps(start, 0);
        this.direction = DOWN;
        // System.out.println("starting direction:" + this.direction);
        
    }

    public abstract <K extends Path2d> K generate(Coordinate2d start, Coordinate2d end, CELL[][] searchSpace);
    
    public enum Direction {
        UP(), DOWN(), LEFT(), RIGHT(), NOTHING();
    }

    @Override
    public final CELL get(Coordinate2d node) {
        return this.searchSpace[node.y][node.x];
    }

    @Override
    public final Integer getSteps(Coordinate2d node) {
        return this.traversalSpace[node.y][node.x];
    }

    @Override
    public final void setSteps(Coordinate2d node, int val) {
        if(this.traversalSpace.length == 0 || node == null){
            return;
        }
        this.traversalSpace[node.y][node.x] = val;
    }

    // Prunes the path by backtracking
    public final Coordinate2d[] backtrackMaze() {
        scoreMaze();
        // start at end, find the path backwards by picking the minimum of neighbors.
        // System.out.println("this.end:" + this.end);
        List<Coordinate2d> pruned = new ArrayList();
        Coordinate2d curr = this.end;
        pruned.add(curr);
        while(this.start.equals(curr) == false){
            int minVal = Integer.MAX_VALUE;
            Coordinate2d minNeighbor = null;
            for(Coordinate2d neighbor : curr.getNeighbors()){
                if(this.getSteps(neighbor) == null){
                    continue;
                }
                if(minVal > this.getSteps(neighbor)){
                    minVal = this.getSteps(neighbor);
                    minNeighbor = neighbor;
                }
            }
            curr = minNeighbor;
            pruned.add(curr);
        }
        List<Coordinate2d> flipped = new ArrayList();
        for(int i = pruned.size() - 1; i >= 0; i --){
            flipped.add(pruned.get(i));
        }
        pruned = flipped;
        // System.out.println("pruned:" + pruned);
        return new ArrayList<Coordinate2d>(pruned).toArray(new Coordinate2d[pruned.size()]);

    }

    //Assumes completable
    public final void scoreMaze() {
        // cycle through entire step cycle
        while (this.current.equals(end) == false) {
            Coordinate2d next = this.step();
            // System.out.println(next);
        }
        Stack<Coordinate2d> visitedStack = new Stack();
        for (Coordinate2d curr : this.getVisitedRepresentation()) {
            if (visitedStack.contains(curr) == false) {
                visitedStack.push(curr);
            }
        }
        if(visitedStack.contains(this.end) == false){
            visitedStack.push(this.end);
        }
        this.visited = visitedStack;

        final class Node {

            final Coordinate2d val;
            final List<Node> adj;

            public Node(Coordinate2d val, List<Node> adj) {
                this.val = val;
                this.adj = adj;
            }

        }
        List<Node> nodeList = new ArrayList();
        for (Coordinate2d curr : this.visited) {
            nodeList.add(new Node(curr, new ArrayList()));
        }
        for (Node n : nodeList) {
            Coordinate2d curr = n.val;
            List<Coordinate2d> surr_intersect = (Arrays.asList(curr.getNeighbors())).stream().filter(this.visited::contains).collect(Collectors.toList());
            for (Node m : nodeList) {
                if (surr_intersect.contains(m.val)) {
                    n.adj.add(m);
                }
            }
        }
        for (Node n : nodeList) {
            for (Node neighbor : n.adj) {
                if (this.getSteps(neighbor.val) == null) {
                    this.setSteps(neighbor.val, this.getSteps(n.val) + 1);
                } else {
                    this.setSteps(neighbor.val, Math.min(this.getSteps(n.val) + 1, this.getSteps(neighbor.val)));
                }
            }
        }
        List<Integer> stepList = new ArrayList();
        for (Coordinate2d curr : this.visited) {
            stepList.add(this.getSteps(curr));
        }
         this.printMaze();
         System.out.println(this.visited);
         System.out.println(stepList);
         this.printScoredMaze();

    }

    public static Direction directionOpposite(Direction input) {
        Direction retDirection = null;
        switch (input) {
            case UP:
                retDirection = DOWN;
                break;
            case DOWN:
                retDirection = UP;
                break;
            case LEFT:
                retDirection = RIGHT;
                break;
            case RIGHT:
                retDirection = LEFT;
                break;
            default:
                retDirection = null;
        }
        return retDirection;
    }

    protected final void rotateRight() {
        switch (this.direction) {
            case UP:
                this.direction = RIGHT;
                break;
            case DOWN:
                this.direction = LEFT;
                break;
            case LEFT:
                this.direction = UP;
                break;
            case RIGHT:
                this.direction = DOWN;
                break;
            default:
                this.direction = null;
        }
        // System.out.println("newDirection:" + this.direction);
    }

    protected final void rotateLeft() {
        switch (this.direction) {
            case UP:
                this.direction = LEFT;
                break;
            case DOWN:
                this.direction = RIGHT;
                break;
            case LEFT:
                this.direction = DOWN;
                break;
            case RIGHT:
                this.direction = UP;
                break;
            default:
                this.direction = null;
        }
        // System.out.println("newDirection:" + this.direction);
    }

    protected final Coordinate2d leftNode() {
        Coordinate2d[] neighbors = this.current.getNeighbors();
        Coordinate2d map_up = neighbors[0], map_down = neighbors[1], map_left = neighbors[2], map_right = neighbors[3];
        switch (this.direction) {
            case UP:
                return map_left;
            case DOWN:
                return map_right;
            case LEFT:
                return map_down;
            case RIGHT:
                return map_up;
            default:
                return null;
        }
    }

    protected final Coordinate2d rightNode() {
        Coordinate2d[] neighbors = this.current.getNeighbors();
        Coordinate2d map_up = neighbors[0], map_down = neighbors[1], map_left = neighbors[2], map_right = neighbors[3];
        switch (this.direction) {
            case UP:
                return map_right;
            case DOWN:
                return map_left;
            case LEFT:
                return map_up;
            case RIGHT:
                return map_down;
            default:
                return null;
        }
    }

    protected final Coordinate2d frontNode() {
        Coordinate2d[] neighbors = this.current.getNeighbors();
        Coordinate2d map_up = neighbors[0], map_down = neighbors[1], map_left = neighbors[2], map_right = neighbors[3];
        switch (this.direction) {
            case UP:
                return map_up;
            case DOWN:
                return map_down;
            case LEFT:
                return map_left;
            case RIGHT:
                return map_right;
            default:
                return null;
        }
    }

    public Coordinate2d[] findNeighbors() {
        ArrayList<Coordinate2d> neighbors = new ArrayList<>();
        if (!isWall(this.leftNode())) {
            neighbors.add(this.leftNode());
        }
        if (!isWall(this.frontNode())) {
            neighbors.add(this.frontNode());
        }
        if (!isWall(this.rightNode())) {
            neighbors.add(this.rightNode());
        }
        if (!isWall(this.backNode())) {
            neighbors.add(this.backNode());
        }
        return neighbors.toArray(new Coordinate2d[neighbors.size()]);
    }

    public final Coordinate2d backNode() {
        Coordinate2d[] neighbors = this.current.getNeighbors();
        Coordinate2d map_up = neighbors[0], map_down = neighbors[1], map_left = neighbors[2], map_right = neighbors[3];
        switch (this.direction) {
            case UP:
                return map_down;
            case DOWN:
                return map_up;
            case LEFT:
                return map_right;
            case RIGHT:
                return map_left;
            default:
                return null;
        }
    }

    @Override
    public final void printMaze() {
        for (CELL[] ROW : this.searchSpace) {
            System.out.println(Arrays.deepToString(ROW));
        }
    }
    
    public void printScoredMaze(){
        for(Integer[] ROW : this.traversalSpace){
            for(int i = 0; i<ROW.length; i++){
                System.out.print(ROW[i] + "\t");
            }
            System.out.println("");
        }
    }

}
