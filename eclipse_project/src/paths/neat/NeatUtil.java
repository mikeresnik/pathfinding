package paths.neat;

import java.io.File;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.anji.integration.Activator;
import com.anji.integration.ActivatorTranscriber;
import com.anji.neat.Evolver;
import com.anji.persistence.Persistence;
import com.anji.util.DummyConfiguration;
import com.anji.util.Properties;

import paths.MazeGenerator;
import paths.coordinates.Coordinate2d;
import util.images;

public final class NeatUtil {
	private NeatUtil() {
		
	}
	

    public static String findMaxChromosome() {
    	try {
    		File fXmlFile = new File("nevt/fitness/fitness.xml");
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		Document doc = dBuilder.parse(fXmlFile);
    				
    		//optional, but recommended
    		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
    		doc.getDocumentElement().normalize();
    		NodeList nodeList =	doc.getElementsByTagName("generation");
    		Element maxGenNode = null;
    		double maxFitness = 0.0;
    		String maxFitness_s = null;
    		for(int i = 0; i < nodeList.getLength(); i++) {
    			Node nNode = nodeList.item(i);
    			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
    				Element generation = (Element) nNode;
    				Element fitness = (Element) generation.getElementsByTagName("fitness").item(0);
    				String max = ((Element)fitness.getElementsByTagName("max").item(0)).getTextContent();
    				double maxVal = Integer.parseInt(max);
    				if(maxVal > maxFitness) {
    					maxGenNode = generation;
        				maxFitness = maxVal;
        				maxFitness_s = max;
    				}

    			}
    		}
    		NodeList maxSpecies = maxGenNode.getElementsByTagName("specie");
    		String chromID = null;
    		for(int index = 0; index< maxSpecies.getLength() && chromID == null; index++) {
    			Element speciesNode =  (Element) maxSpecies.item(index);
    			NodeList chromosomes = speciesNode.getElementsByTagName("chromosome");
    			for(int chromIndex = 0; chromIndex < chromosomes.getLength(); chromIndex++) {
    				Element chrom = (Element) chromosomes.item(chromIndex);
    				if(chrom.getAttribute("fitness").equals(maxFitness_s)) {
    					chromID = chrom.getAttribute("id");
    					break;
    				}
    			}
    		}
    		System.out.println("totalChamp:" + chromID);
    		return chromID;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
    
	public static void evolveProperties(String propertiesFile) {
    	Logger logger = Logger.getLogger( Evolver.class );
    	try {
    		Properties props = new Properties( propertiesFile);
    		Evolver evolver = new Evolver();
    		evolver.init( props );
    		evolver.run();
    	}
    	catch ( Throwable th ) {
    		logger.error( "", th );
    		th.printStackTrace();
    	}
	}
	
	public static Activator getActivatorMax() {
		return getActivator(findMaxChromosome());
	}

    public static Activator getActivator(String chromosomeID) {
    	String propertiesFile = "properties/maze_test.properties";
    	try {
    		Configuration config = new DummyConfiguration();
			Properties props = new Properties( propertiesFile );
			Persistence 
			db = (Persistence) props.singletonObjectProperty( Persistence.PERSISTENCE_CLASS_KEY );
    		Chromosome chrom = db.loadChromosome( chromosomeID, config );
    		ActivatorTranscriber activatorFactory = (ActivatorTranscriber) props
					.singletonObjectProperty( ActivatorTranscriber.class );
    		if(chrom == null) {
    			return null;
    		}
    		Activator activator = activatorFactory.newActivator( chrom );
    		return activator;
    	}catch(Throwable th) {
    		th.printStackTrace();
        	return null;
    	}
    }
    
    

}
