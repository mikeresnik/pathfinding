package paths.neat;

import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import org.jgap.Chromosome;

import com.anji.integration.Activator;
import com.anji.integration.ErrorFunction;
import com.anji.integration.TranscriberException;
import com.anji.neat.NeatTargetFitnessFunction;
import com.anji.util.Properties;

import paths.MazeGenerator;
import paths.coordinates.Coordinate2d;

public class MazeTargetFitnessFunction extends NeatTargetFitnessFunction {

	public static int size = 100;
	protected static int MAX_FITNESS = 2*size;
	public static Character[][] mazeCharacter_s;
	public static Coordinate2d start_s, end_s;
	
	
	public void init( Properties newProps ) {
		try {
			super.init( newProps );
			setMaxFitnessValue( MAX_FITNESS );
		}
		catch ( Exception e ) {
			throw new IllegalArgumentException( "invalid properties: " + e.getClass().toString()
					+ ": " + e.getMessage() );
		}
	}
	
	@Override
	public void evaluate(List genotypes) {
		System.out.println("EVALUATE:" + genotypes.size());
		Iterator it = genotypes.iterator();

		int size = 100;
        int max_fitness = Integer.MIN_VALUE;
        NeatPath2d max_path = null;
		while (it.hasNext()) {
			Chromosome genotype = (Chromosome) it.next();
			try {
				Activator activator = activatorFactory.newActivator(genotype);
				// TODO
				/**
				 * NeatPath2d path;
				 * f = path.calculateFitness();
				 */
				NeatPath2d path = new NeatPath2d(start_s, end_s, mazeCharacter_s,  activator);
				
				int f = MAX_FITNESS + path.calculateFitness();
				genotype.setFitnessValue(f);
				if(f > max_fitness) {
					max_fitness = f;
					max_path = path;
				}
				// System.out.println(path.getVisitedRepresentation());
			} catch (TranscriberException e) {
				logger.warn("transcriber error: " + e.getMessage());
				genotype.setFitnessValue(1);
			}
		}
		System.out.println(max_path.current);
	}

	
	// method to generate gif from all champion chromosomes.
	
	/**
	 * generate path from max chromosome
	 * 
	 * 
	 */
	
	public static void setVals(Coordinate2d start, Coordinate2d end, Character[][] mazeCharacter) {
		start_s = start;
		end_s = end;
		mazeCharacter_s = mazeCharacter;
		size = mazeCharacter.length;
		MAX_FITNESS = 2*size;
	}

	@Override
	protected int calculateErrorFitness(double[][] responses, double minResponse, double maxResponse) {
		// TODO Auto-generated method stub
		// being called
		System.out.println("called");
		return 0;
	}

}
