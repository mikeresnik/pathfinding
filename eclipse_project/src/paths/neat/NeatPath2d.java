package paths.neat;

import java.util.ArrayList;
import java.util.List;

import com.anji.integration.Activator;

import paths.MazeException;
import paths.Path2d;
import paths.coordinates.Coordinate2d;

public class NeatPath2d extends Path2d<Character> {

	Activator activator;

	public NeatPath2d(Coordinate2d start, Coordinate2d end, Character[][] searchSpace, 
			Activator activator) {
		super(start, end, searchSpace, 'W');
		this.activator = activator;
		// System.out.println("new NeatPath2d");
		// TODO Auto-generated constructor stub
	}

	@Override
	public NeatPath2d generate(Coordinate2d start, Coordinate2d end, Character[][] searchSpace) {
		return new NeatPath2d(start, end, searchSpace,  this.activator);
	}

	@Override
	protected void initCollection() {
		// TODO Auto-generated method stub
        this.visited.push(start);
        this.traversed.add(start);

	}

	@Override
	public List<Coordinate2d> getVisitedRepresentation() {
		return this.visited;
	}

	@Override
	public List<Coordinate2d> getTraversedRepresentation() {
		return this.traversed;
	}

	private Coordinate2d evaluateInstruction(double[] instruction) {
		Coordinate2d retCoord = null;
		// System.out.println(Arrays.toString(instruction));
		int max_index = -1;
		double max_value = -2.0;
		for(int index =0; index < instruction.length; index++) {
			double curr_value = instruction[index];
			if(curr_value > max_value) {
				max_index = index;
				max_value = curr_value;
			}
		}
		Coordinate2d neighbor = this.current.getNeighbors()[max_index];
		if(this.isWall(neighbor) == false ) {
			retCoord = neighbor;
		}else {
			retCoord = this.current;
		}
		
		return retCoord;
	}

	@Override
	public Coordinate2d step() throws MazeException {
		/**
		 * stimulus = findStimulus(); // Surroundings instruction =
		 * this.activator.activate(stimulus); next =
		 * this.evaluateInstruction(instruction); curr = next; updateVT();
		 */

		double[] stimulus = this.findStimulus();
		double[] instruction = this.activator.next(stimulus);
		Coordinate2d next = this.evaluateInstruction(instruction);
		this.current = next;
		boolean travCont = this.traversed.contains(this.current);
		boolean vistCont = this.visited.contains(this.current);

		if (travCont == false) {
			this.traversed.add(this.current);
		}
		if (vistCont) {
			this.visited.remove(this.current);
		} else {
			this.visited.add(this.current);
		}

		return this.current;
	}

	public double[] findStimulus() {
		/**
		 * [0] [1] surr [2] [3]
		 * 
		 * [4] [5] dir
		 * 
		 * [6] dist
		 * 
		 */

		// quantify surroundings
		List<Character> values = this.current.getNeighborValues(this.searchSpace);
		double[] surr = new double[values.size()];
		for (int index = 0; index < values.size(); index++) {
			Coordinate2d neighbor = this.current.getNeighbors()[index];
			Double val = null;
			char curr_char = values.get(index);
			switch(curr_char) {
			case 'C':
				val = 1.0;
				if(this.traversed.contains(neighbor) || this.visited.contains(neighbor)) {
					val = 0.0;
				}
				break;
			case 'W':
				val = -1.0;
				break;
			default:
				val = -1.0;
			}
			surr[index] = val;
		}
		double[] dir = this.end.betweenDelta(current);
		
		double normalDistance = Coordinate2d.realDistance(this.current, this.end) / Coordinate2d.realDistance(this.start, this.end);
		normalDistance = (normalDistance > 1 ? 1.0 : normalDistance);
		
		double[] dist = new double[] {normalDistance};
		
		
		double[] retArray = new double[surr.length + dir.length + dist.length];
		
		
		int surr_index = 0;
		for(int i = 0; i < surr.length; i++) {
			retArray[surr_index + i] = surr[i];
		}
		
		int dir_index =surr_index + surr.length;
		for(int j = 0; j < dir.length; j++) {
			retArray[dir_index + j] = dir[j];
		}
		
		int dist_index = dir_index + dist.length;
		retArray[dist_index] = normalDistance;
		

		return retArray;
	}

	public int calculateFitness() {

		int threshold = (int) Math.pow(this.searchSpace.length * this.searchSpace[0].length, 1.1);

		int counter;
		for (counter = 0; counter < threshold && (this.end.equals(current) == false); counter++) {
			this.step();
		}
		/**
		 * while counter < thresh && (curr.equals(end) == false) ****this.step();
		 *
		 * # Bias the fitness function with a known best fit line?
		 *
		 * # Trying to minimize penalties. penalties = finalDist + traversed.length
		 * return -1 * penalties;
		 *
		 */
		this.justifyVisited();
		int penalties = (int) Coordinate2d.realDistance(this.current, this.end);

		return -1*penalties;
	}

	private void justifyVisited() {
		
		for(Coordinate2d trav : this.traversed) {
			Coordinate2d[] neighbors = trav.getNeighbors();
			int count = 0;
			for(Coordinate2d neighbor : neighbors) {
				if(this.visited.contains(neighbor)) {
					count++;
				}
			}
			if(count >= 2) {
				this.visited.add(trav);
			}
			
		}
		List<Coordinate2d> rem = new ArrayList();
		for(Coordinate2d vis : this.visited) {
			Coordinate2d[] neighbors = vis.getNeighbors();
			int count = 0;
			for(Coordinate2d neighbor: neighbors) {
				if(this.visited.contains(neighbor)) {
					count++;
				}
			}
			if(count < 2) {
				rem.add(vis);
			}
		}
		for(Coordinate2d curr : rem) {
			visited.remove(curr);
		}
		
	}
	
}
