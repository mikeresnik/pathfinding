package paths;

public class MazeException extends IllegalStateException {

    public static final MazeException CANNOT_COMPLETE = new MazeException("Maze cannot be completed.")
            , NOT_COMPLETED = new MazeException("Maze isn't completed.");

    protected MazeException() {
        super(); // Redundant
    }

    public MazeException(String cause) {
        super(cause);
    }

}
