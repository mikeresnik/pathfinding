package paths.coordinates;

import java.util.ArrayList;
import java.util.List;
import paths.Path2d.Direction;
import paths.dimensions.Dimension;
import paths.dimensions.Dimension2d;

public class Coordinate2d extends Coordinate<Coordinate2d, Dimension2d> {

    public int x, y;

    public Coordinate2d(int x, int y) {
        super(x, y);
        this.x = x;
        this.y = y;
    }

    @Override
    public Coordinate2d[] getNeighbors() {
        Coordinate2d[] retArray = new Coordinate2d[]{new Coordinate2d(this.x, this.y - 1), new Coordinate2d(this.x, this.y + 1), new Coordinate2d(this.x - 1, this.y), new Coordinate2d(this.x + 1, this.y)};
        return retArray;
    }

    @Override
    public Integer[] getNeighborValues(Dimension2d dimension, ArrayList<Integer> searchSpace) {
        Dimension.justifyDimensions(dimension, searchSpace);
        Integer[] retArray = new Integer[]{null, null, null, null};
        Coordinate2d[] neighbors = this.getNeighbors();
        for (int y_index = 0; y_index < dimension.l; y_index++) {
            for (int x_index = 0; x_index < dimension.w; x_index++) {
                Integer curr = searchSpace.get(y_index * dimension.l + x_index);
                for (int i = 0; i < neighbors.length; i++) {
                    if (x_index == neighbors[i].x && y_index == neighbors[i].y) {
                        retArray[i] = curr;
                    }
                }
            }
        }
        return retArray;
    }

    public <T> List<T> getNeighborValues(T[][] searchSpace) {
        Coordinate2d[] neighbors = this.getNeighbors();
        List<T> retList = new ArrayList();
        for (int i = 0; i < neighbors.length; i++) {
            try {
                retList.add(searchSpace[neighbors[i].y][neighbors[i].x]);
            } catch (Exception e) {
                retList.add(null);
            }
        }
        return retList;
    }

    @Override
    public String toString() {
        return "Coordinate2d{" + "x=" + x + ", y=" + y + '}';
    }

    public Direction between(Coordinate2d other) {
        int dx = other.y - this.y;
        int dy = other.x - this.x;

        double dl = Math.sqrt(dx * dx + dy * dy);

        double dx_norm = dx / dl, dy_norm = dy / dl;
        if (Math.abs(dx_norm) > Math.abs(dy_norm)) {
            if (dx > 0) {
                return Direction.RIGHT;
            } else {
                return Direction.LEFT;
            }
        } else {
            if (dy > 0) {
                return Direction.DOWN;
            } else {
                return Direction.UP;
            }
        }

    }
    public double[] betweenDelta(Coordinate2d other) {
        int dx = other.y - this.y;
        int dy = other.x - this.x;

        double dl = realDistance(this, other);

        double dx_norm = dx / dl, dy_norm = dy / dl;
        return new double[] {dx_norm, dy_norm};
    }

    public static int manhattanDistance(Coordinate2d node1, Coordinate2d node2) {
        return Math.abs(node1.x - node2.x) + Math.abs(node1.y - node2.y);
    }

    public static double realDistance(Coordinate2d node1, Coordinate2d node2) {
        return Math.sqrt(Math.pow((node1.x - node2.x), 2) + Math.pow((node1.y - node2.y), 2));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.x;
        hash = 53 * hash + this.y;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordinate2d other = (Coordinate2d) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        return true;
    }

}
