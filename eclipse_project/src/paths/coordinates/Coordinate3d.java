package paths.coordinates;

import java.util.ArrayList;
import paths.dimensions.Dimension;
import paths.dimensions.Dimension3d;

public class Coordinate3d extends Coordinate<Coordinate3d, Dimension3d> {

    public final int x, y, z;

    public Coordinate3d(int x, int y, int z) {
        super(x, y, z);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public Coordinate3d[] getNeighbors() {
        Coordinate3d[] retArray = new Coordinate3d[]{
            new Coordinate3d(this.x, this.y - 1, this.z), new Coordinate3d(this.x, this.y + 1, this.z),
            new Coordinate3d(this.x - 1, this.y, this.z), new Coordinate3d(this.x + 1, this.y, this.z),
            new Coordinate3d(this.x, this.y, this.z - 1), new Coordinate3d(this.x, this.y, this.z + 1)
        };
        return retArray;
    }

    @Override
    public Integer[] getNeighborValues(Dimension3d dimension, ArrayList<Integer> searchSpace) {
        Dimension.justifyDimensions(dimension, searchSpace);
        Integer[] retArray = new Integer[]{null, null, null, null, null, null};
        Coordinate3d[] neighbors = this.getNeighbors();
        for (int z_index = 0; z_index < dimension.h; z_index++) {
            for (int y_index = 0; y_index < dimension.l; y_index++) {
                for (int x_index = 0; x_index < dimension.w; x_index++) {
                    Integer curr = searchSpace.get(z_index * dimension.h + y_index * dimension.l + x_index);
                    for (int i = 0; i < neighbors.length; i++) {
                        if (neighbors[i].equals(new Coordinate3d(x_index, y_index, z_index))) {
                            retArray[i] = curr;
                            break;
                        }
                    }
                }
            }
        }
        return retArray;
    }

}
