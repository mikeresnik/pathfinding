/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paths.Genetics;

import genetics.BreedingObject;
import paths.MikeGeneticParse;
import static paths.MikeGeneticParse.nextNode;
import paths.Path2d;
import paths.coordinates.Coordinate2d;

/**
 *
 * @author Hadi Soufi
 */
public class PathBreeder extends BreedingObject<Genetic2d.Direction, Double> {

    private final Coordinate2d START, END;
    private final Path2d<Character> path;
    private boolean isActive;
    private double thresh = 2;
    
    public PathBreeder(int CHROMOSOMELENGTH, double MUTATIONRATE, int POPULATIONSIZE, Path2d<Character> path) {
        super(CHROMOSOMELENGTH, MUTATIONRATE, POPULATIONSIZE, Genetic2d.Direction.values());
        this.START = path.start;
        this.END = path.end;
        this.path = path;
        this.isActive = true;
    }

    @Override
    public Double getFitness(Path2d.Direction[] input) {
        Coordinate2d prev = null;
        Coordinate2d curr = this.START;
        double penalties = 0;
        for (Path2d.Direction d : input) {
            prev = curr;
            curr = nextNode(curr, d);
            if(this.path.isWall(curr)){
                curr = prev;
            }
        }
        double dist = Coordinate2d.manhattanDistance(curr, END);
        if(dist <=thresh){
            this.isActive = false;
        }
        return 1.0/(dist);
    }

    @Override
    public boolean stopConditionMet() {
        boolean retBool = !isActive || MikeGeneticParse.compileDirections(START, this.findMostFitIndividual()).equals(END);
        System.out.println("continue:" + retBool);
        return retBool;
    }
    
}
