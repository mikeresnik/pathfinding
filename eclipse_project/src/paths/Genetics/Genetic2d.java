package paths.Genetics;

import java.util.List;
import paths.MazeException;
import paths.MazeGenerator;
import paths.Path2d;
import paths.coordinates.Coordinate2d;


public class Genetic2d extends Path2d<Character>{
    
    public PathBreeder breeder;
    
    public Genetic2d(Coordinate2d start, Coordinate2d end, char[][] searchSpace) {
        this(start, end, MazeGenerator.Util.charToCharacter(searchSpace));
    }

    public Genetic2d(Coordinate2d start, Coordinate2d end, Character[][] searchSpace) {
        super(start, end, searchSpace, 'W');
        System.out.println("Ginit");
        init(start, end);
    }

    private void init(Coordinate2d start, Coordinate2d end) {
        breeder = new PathBreeder(3*this.searchSpace.length, 0.50, 20*this.searchSpace.length, this);
        System.out.println("breeder made");
    }
    
    @Override
    protected void initCollection() {
        
    }

    @Override
    public List<Coordinate2d> getVisitedRepresentation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Coordinate2d> getTraversedRepresentation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Coordinate2d step() throws MazeException {
        System.out.println("PStep");
        this.breeder.step();
        return null;
    }
    
    public void run(int gens) throws MazeException{
        for(int i = 0; i < gens; i++){
            if(this.breeder.stopConditionMet()){
                return;
            }
            this.breeder.step();
        }
    }
    
    public Coordinate2d nextFromGrammar(){
        // decision stuff
        return null; // nextCoord
    }

    @Override
    public Genetic2d generate(Coordinate2d start, Coordinate2d end, Character[][] searchSpace) {
        return new Genetic2d(start, end, searchSpace);
    }
}
