package com.anji.neat;

import com.anji.integration.ErrorFunction;
import com.anji.integration.TargetFitnessFunction;
import com.anji.util.Properties;

public class TestFitnessFunction extends TargetFitnessFunction{

	protected final static boolean SUM_OF_SQUARES = false;

	protected final static int MAX_FITNESS = 2;

	/**
	 * See <a href=" {@docRoot}/params.htm" target="anji_params">Parameter Details </a> for
	 * specific property settings.
	 * 
	 * @param newProps configuration parameters
	 */
	public void init( Properties newProps ) {
		try {
			super.init( newProps );
			ErrorFunction.getInstance().init( newProps );
			setMaxFitnessValue( MAX_FITNESS );
		}
		catch ( Exception e ) {
			throw new IllegalArgumentException( "invalid properties: " + e.getClass().toString()
					+ ": " + e.getMessage() );
		}
	}

	/**
	 * Subtract <code>responses</code> from targets, sum all differences, subtract from max
	 * fitness, and square result.
	 * 
	 * @param responses output top be compared to targets
	 * @param minResponse
	 * @param maxResponse
	 * @return result of calculation
	 */
	protected int calculateErrorFitness( double[][] responses, double minResponse, double maxResponse ) {
//		System.out.println("Calculating Error Fitness");
//		double maxSumDiff = ErrorFunction.getInstance().getMaxError(
//				getTargets().length * getTargets()[ 0 ].length,
//				( maxResponse - minResponse ), SUM_OF_SQUARES );
//		double maxRawFitnessValue = Math.pow( maxSumDiff, 2 );
//
//		double sumDiff = ErrorFunction.getInstance().calculateError( getTargets(), responses, false );
//		if ( sumDiff > maxSumDiff )
//			throw new IllegalStateException( "sum diff > max sum diff" );
//		double rawFitnessValue = Math.pow( maxSumDiff - sumDiff, 2 );
//		double skewedFitness = ( rawFitnessValue / maxRawFitnessValue ) * MAX_FITNESS;
//		int result = (int) skewedFitness;
//		return result;
		return 1;
	}
}
