package genetics;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import paths.coordinates.Coordinate3d;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mike
 * @param <T> alphabet type
 * @param <K> fitness type
 */
public abstract class BreedingObject<T, K extends Number> {

    protected final T[] alphabet;
    protected final Class<?> CHILDCLASS;
    public final int POPULATIONSIZE, CHROMOSOMELENGTH;
    public final double MUTATIONRATE;
    protected List<T[]> population;
    protected List<Pair<Integer, Double>> breedPool, accumulatedPool;
    protected PriorityQueue<T[]> topPicks, bottomPicks;
    protected double GEN_CROSSOVER = 20.0/100.0;
    protected int GENERATION;
    public static final boolean DEBUG = true;

    public BreedingObject(int CHROMOSOMELENGTH, double MUTATIONRATE, int POPULATIONSIZE, T[] alphabet) {
        this.alphabet = alphabet;
        this.CHILDCLASS = this.alphabet[0].getClass();
        this.CHROMOSOMELENGTH = CHROMOSOMELENGTH;
        this.MUTATIONRATE = MUTATIONRATE;
        this.POPULATIONSIZE = POPULATIONSIZE;
        this.initializePopulation();
    }

    public abstract K getFitness(T[] input);

    public abstract boolean stopConditionMet();

    private void initializePopulation() {
        System.out.println("Initializing Population");
        this.population = new ArrayList(POPULATIONSIZE);
        @SuppressWarnings("unchecked")
        T[] child;
        for (int POPINDEX = 0; POPINDEX < POPULATIONSIZE; POPINDEX++) {
            child = (T[]) Array.newInstance(CHILDCLASS, CHROMOSOMELENGTH);
            for (int CHILDINDEX = 0; CHILDINDEX < CHROMOSOMELENGTH; CHILDINDEX++) {
                child[CHILDINDEX] = this.randomAlphabetValue();
            }
            this.population.add(child);
        }
        System.out.println("Population Initialized");

    }

    public T randomAlphabetValue() {
        return alphabet[(int) (Math.random() * alphabet.length)];
    }

    public void printPopulation() {
        this.population.forEach((child) -> {
            System.out.println(Arrays.toString(child));
        });
    }

    public void mutate(int pIndex) {
        T[] parent = this.population.get(pIndex);
        T[] newChild = (T[]) Array.newInstance(CHILDCLASS, CHROMOSOMELENGTH);
        for (int CHROMINDEX = 0; CHROMINDEX < parent.length; CHROMINDEX++) {
            newChild[CHROMINDEX] = (Math.random() <= this.MUTATIONRATE ? this.randomAlphabetValue() : parent[CHROMINDEX]);
        }
        this.population.set(pIndex, newChild);
    }

    protected Pair<T[], T[]> crossover(int pIndex1, int pIndex2) {
        T[] chromosome1 = this.population.get(pIndex1);
        T[] chromosome2 = this.population.get(pIndex2);
        int crossOverPoint = (int) (Math.random() * this.CHROMOSOMELENGTH);
        for (int CHROMINDEX = crossOverPoint; CHROMINDEX < this.CHROMOSOMELENGTH; CHROMINDEX++) {
            T temp = chromosome1[CHROMINDEX];
            chromosome1[CHROMINDEX] = chromosome2[CHROMINDEX];
            chromosome2[CHROMINDEX] = temp;
        }
        return new Pair(chromosome1, chromosome2);
    }

    public double scorePopulation() {
        double totalFitness = 0.0;
        for (T[] chromosome : this.population) {
            totalFitness += this.getFitness(chromosome).doubleValue();
        }
        return totalFitness;
    }

    public int findMostFitIndex() {
        double maxFitness = Double.MIN_VALUE;
        int maxIndex = -1;
        for (int popIndex = 0; popIndex < this.population.size(); popIndex++) {
            T[] current = this.population.get(popIndex);
            K fitness = this.getFitness(current);
            if (fitness.doubleValue() > maxFitness) {
                maxFitness = fitness.doubleValue();
                maxIndex = popIndex;
            }
        }
        return maxIndex;
    }

    public T[] findMostFitIndividual() {
        return this.population.get(this.findMostFitIndex());
    }

    public void initPools() {
        this.breedPool = new ArrayList();
        this.accumulatedPool = new ArrayList();
        
        // number of generational crossovers     
        
        
        double totalFitness = this.scorePopulation();
        double runningTotal = 0.0;
//        
//        topPicks = new PriorityQueue(new Comparator<T[]>(){
//            @Override
//            public int compare(T[] t, T[] t1) {
//                K f1 = getFitness(t), f2 = getFitness(t1);
//                return Double.compare(f1.doubleValue(), f2.doubleValue());
//            }
//        });
//        
//        for (int INDEV_i = 0; INDEV_i < this.population.size(); INDEV_i++) {
//            T[] individual = this.population.get(INDEV_i);
//            topPicks.add(individual);
//        }
//        
        
        
        
        
        for (int INDEV_i = 0; INDEV_i < this.population.size(); INDEV_i++) {
            T[] individual = this.population.get(INDEV_i);
            K fitness = this.getFitness(individual);
            double prob = fitness.doubleValue() / totalFitness;
            runningTotal += prob;
            if (prob != 0.0) {
                Pair<Integer, Double> breedPair = new Pair(INDEV_i, prob); //pdf
                Pair<Integer, Double> accumulatedPair = new Pair(INDEV_i, runningTotal); // cdf
                breedPool.add(breedPair);
                accumulatedPool.add(accumulatedPair);
            }
        }
    }

    public int getParent() {
        double rand = Math.random();
        for (int i = 0; i < this.accumulatedPool.size(); i++) {
            if (rand <= this.accumulatedPool.get(i).getValue()) {
                return this.accumulatedPool.get(i).getKey();
            }
        }
        throw new RuntimeException("Cannot find valid parent.");
    }

    public void breedPopulation() {
        this.initPools();
        System.out.println("Pools initialized.");
        // topPicks & bottomPicks found
        
        
        List<T[]> newPopulation = new ArrayList();
        for (int i = 0; i < this.population.size() / 2; i++) {
            int p1Index = this.getParent(), p2Index = this.getParent();
            Pair<T[], T[]> crossPair = this.crossover(p1Index, p2Index);
            newPopulation.add(crossPair.getKey());
            newPopulation.add(crossPair.getValue());
        }
//        
//        bottomPicks =new PriorityQueue(new Comparator<T[]>(){
//            @Override
//            public int compare(T[] t, T[] t1) {
//                K f1 = getFitness(t), f2 = getFitness(t1);
//                return -1*Double.compare(f1.doubleValue(), f2.doubleValue());
//            }
//        });
//        
////        for (int INDEV_i = 0; INDEV_i < newPopulation.size(); INDEV_i++) {
////            T[] individual = newPopulation.get(INDEV_i);
////            bottomPicks.add(individual);
////        }
//        
//        int num_highest = (int) Math.ceil(this.POPULATIONSIZE * GEN_CROSSOVER);
//        System.out.println("NUM_HIGHEST:" + num_highest);
//        for (int i = 0; i < num_highest; i++) {
//            // remove lowest from current
//            newPopulation.remove(bottomPicks.poll());
//            // add highest from previous
//            newPopulation.add(topPicks.poll());
//        }
        
        
        // top picks --> new
        this.population = newPopulation;
    }

    public void run() {
        this.GENERATION = 1;
        this.initializePopulation();
        while (this.stopConditionMet() == false) {
            this.step();
            if (GENERATION > 10000) {
                break;
            }
        }
    }

    public void step() {
        this.breedPopulation();
        if (DEBUG) {
            T[] fittest = this.findMostFitIndividual();
            K number = this.getFitness(fittest);
            System.out.printf("GENERATION:%s \t CHROMOSOME:%s \t Fitness:%s\n", this.GENERATION, Arrays.toString(fittest), number);
        }
        this.GENERATION++;
    }

    public static void main(String[] args) {
        int POPSIZE = 732, CHROMLEN = 12;
        double MUTRATE = 0.09;
        final Character[] alphabet = new Character[]{'a', 't', 'g', 'c'};

        List<Map<Coordinate3d, Color>> graphData = new ArrayList();
        Map<Coordinate3d, Color> populationData = new HashMap();
        Coordinate3d generationData = new Coordinate3d(0, 0, 0);
        int tests = 3;
        double avgGenerations;
        double minGenerations;
        double bestMutrate;
        double overallBestMutrate = 1;
        double overallMinGenerations = 10000;

        for (int k = 100; k < 110; k++) {
            POPSIZE = k;
            for (int j = 0; j < tests; j++) {
                MUTRATE = 0.09;
                minGenerations = 10000.0;
                bestMutrate = 0;
                avgGenerations = 0;
                while (MUTRATE < 0.5) {
                    MUTRATE += 0.01;

                    BreedingObject<Character, Double> b
                            = new BreedingObject<Character, Double>(CHROMLEN, MUTRATE, POPSIZE, alphabet) {

                        @Override
                        public Double getFitness(Character[] input) {
                            char[] matching = "atgcatgcatgc".toCharArray(); //"atgcatgcatgcatcgacgatgcagtcgactgacgtcagtgcagtcaggcatagactgactgacgtacgtagtagctgacgctgacgactgctgacgatgcatcatgtaggtagcagtgagatcgagcagcagctcgacgcagtcgatcagctgagcacgatgacgcaggatcgactgacgacgcagctgcagcgacgacgacgcagtcagctgactgactgacgcagcagcaggagcacgatgacgcaggatcgactgacgacgcagctgcagcgacgacgacgcagtcagctgactga".toCharArray();
                            int matchInt = 0;
                            for (int CHROMINDEX = 0; CHROMINDEX < CHROMOSOMELENGTH; CHROMINDEX++) {
                                if (input[CHROMINDEX] == matching[CHROMINDEX]) {
                                    matchInt++;
                                }
                            }
                            return (double) matchInt;
                        }

                        @Override
                        public boolean stopConditionMet() {
                            Character[] fittest = this.findMostFitIndividual();
                            return this.getFitness(fittest) == this.CHROMOSOMELENGTH;
                        }
                    };

                    for (int i = 0; i <= tests; i++) {
                        b.run();
                        avgGenerations += b.GENERATION;
                    }
                    avgGenerations /= tests;
                    
                    generationData = new Coordinate3d((int)(MUTRATE * 100), POPSIZE, (int) Math.rint(avgGenerations));
                    System.out.println(generationData);
                }
            }
        }
    }
}
