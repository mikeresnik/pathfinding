
import static paths.MikeGeneticParse.nextNode;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.anji.integration.Activator;
import com.anji.integration.ActivatorTranscriber;
import com.anji.neat.Evolver;
import com.anji.neat.NeatActivator;
import com.anji.persistence.Persistence;
import com.anji.util.DummyConfiguration;
import com.anji.util.Properties;

import paths.BFS2d;
import paths.MazeGenerator;
import paths.Path2d;
import paths.Path2d.Direction;
import paths.Genetics.Genetic2d;
import paths.coordinates.Coordinate2d;
import paths.neat.MazeTargetFitnessFunction;
import paths.neat.NeatPath2d;
import util.files;
import util.images;
import util.math.matrices;

public class Main {
	
    public static void main(String[] args)throws Exception {
    	// testMazeFitnessGIF();
    	testScoreAll();
    	
    	// Test comment
    }
    
    
    
    public static void testMultipleGifs() {
    	int size = 100;
        Queue<char[][]> mazes = MazeGenerator.Load.multiple(size);
        
        for(char[][] maze : mazes) {
            Character[][] mazeCharacter = MazeGenerator.Util.charToCharacter(maze);
            Coordinate2d start = MazeGenerator.Util.findFirst(mazeCharacter, 'C'), end = MazeGenerator.Util.findLast(mazeCharacter, 'C');
        	
        }
    }
    
    public static void testCustomFitnessFunction() {
    	String propertiesFile = "properties/maze_test.properties";
    	testEvolve(propertiesFile);
    }
    
    public static void testMazeFitnessGIF() throws Exception {
    	List<byte[][][]> gifFrames = new ArrayList();
    	String[] champions = findChampChromosomes();
    	for(String chromID : champions) {
    		byte[][][] image = testMazeCustomFitness(chromID, 100);
    		gifFrames.add(image);
    	}
    	images.saveImagesToGif(gifFrames, "maze_test.gif", 200);
    }
    
    public static byte[][][] testMazeCustomFitness(String chromosomeID, int size) {
    	String propertiesFile = "properties/maze_test.properties";
        Queue<char[][]> mazes = MazeGenerator.Load.multiple(size);
        char[][] maze0 = mazes.poll();
        Character[][] maze0Character = MazeGenerator.Util.charToCharacter(maze0);
        Coordinate2d start = MazeGenerator.Util.findFirst(maze0Character, 'C'), end = MazeGenerator.Util.findLast(maze0Character, 'C');
        byte[] red = new byte[] {(byte) 255, (byte) 1, (byte) 1};
        byte[][][] image = new byte[maze0.length][maze0[0].length][];
        for(int ROW =0; ROW < image.length; ROW++) {
        	for(int COL = 0; COL < image.length; COL++) {
        		image[ROW][COL] = images.BLACK_B;
        	}
        }
    	try {
    		Configuration config = new DummyConfiguration();
			Properties props = new Properties( propertiesFile );
			Persistence 
			db = (Persistence) props.singletonObjectProperty( Persistence.PERSISTENCE_CLASS_KEY );
    		Chromosome chrom = db.loadChromosome( chromosomeID, config );
    		ActivatorTranscriber activatorFactory = (ActivatorTranscriber) props
					.singletonObjectProperty( ActivatorTranscriber.class );
    		if(chrom == null) {
    			return null;
    		}
    		Activator activator = activatorFactory.newActivator( chrom );
			NeatPath2d path = new NeatPath2d(start, end, maze0Character, activator);
			path.calculateFitness();
			System.out.println(path.getVisitedRepresentation());
			for(int ROW = 0; ROW < image.length; ROW++) {
				for(int COL = 0; COL < image[0].length; COL++) {
					Coordinate2d coordRep = new Coordinate2d(COL, ROW);
						if(path.isWall(coordRep)) {
							image[ROW][COL] = images.BLACK_B;
						}else {
							image[ROW][COL] = images.WHITE_B;
						}
				}
			}
			for(Coordinate2d loc : path.getTraversedRepresentation()) {
				image[loc.y][loc.x] = new byte[] {(byte) 1, (byte) 1, (byte) 255};
			}
			for(Coordinate2d loc : path.getVisitedRepresentation()) {
				image[loc.y][loc.x] = new byte[] {(byte) 255, (byte) 1, (byte) 1};
			}
			images.saveImageBytes(image, "test.bmp");
    	}catch(Throwable th) {
    		th.printStackTrace();
    	}
    	return image;
    }
    
    
    public static void testNeatXOR(boolean ...doEvolve) {
    	String propertiesFile = "properties/xor.properties";
    	if(doEvolve.length > 0 && doEvolve[0]) {
        	testEvolve(propertiesFile);
    	}
    	// Generate image of 1000x1000 and test values from ROW/1000 to COL/1000
    	double[][] stimuli = new double[1000*1000][];
    	for(int index = 0; index < stimuli.length; index++) {
    		double ROW = index % 1000;
    		double COL = index / 1000;
    		stimuli[index] = new double[] {ROW/1000.0, COL/1000.0}; 
    	}
    	double[][] responses = evaluateMaxChromosome(propertiesFile, stimuli);
    	
    	byte[][][] image = new byte[1000][1000][];
    	for(int ROW = 0; ROW < image.length; ROW++) {
    		for(int COL = 0; COL < image[0].length; COL++) {
    			byte[] pixel = null;
    			double truth = responses[ROW*image.length + COL][0];
    			pixel = matrices.getColorSliding(truth);
    			image[ROW][COL] = pixel;
    		}
    	}
    	try {

        	images.saveImageBytes(image, "properties/xor_visual.bmp");
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    public static double[][] evaluateMaxChromosome(String propertiesFile, double[][] stimuli, boolean ...doPrint) {
    	String chromosomeID = findMaxChromosome();
    	try {
    		NeatActivator na = new NeatActivator();
			Properties props = new Properties( propertiesFile );
			na.init( props );
			if(doPrint.length > 0 && doPrint[0]) {
				String activation = na.activateString(stimuli, chromosomeID);
				System.out.println(activation);
			}
			return na.activateStimuli(stimuli, chromosomeID);
    	}catch(Throwable th) {
    		th.printStackTrace();
    	}    	
    	return null;
    }
    
    public static String findMaxChromosome() {
    	try {
    		File fXmlFile = new File("nevt/fitness/fitness.xml");
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		Document doc = dBuilder.parse(fXmlFile);
    				
    		//optional, but recommended
    		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
    		doc.getDocumentElement().normalize();
    		NodeList nodeList =	doc.getElementsByTagName("generation");
    		Element maxGenNode = null;
    		double maxFitness = 0.0;
    		String maxFitness_s = null;
    		for(int i = 0; i < nodeList.getLength(); i++) {
    			Node nNode = nodeList.item(i);
    			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
    				Element generation = (Element) nNode;
    				Element fitness = (Element) generation.getElementsByTagName("fitness").item(0);
    				String max = ((Element)fitness.getElementsByTagName("max").item(0)).getTextContent();
    				double maxVal = Integer.parseInt(max);
    				if(maxVal > maxFitness) {
    					maxGenNode = generation;
        				maxFitness = maxVal;
        				maxFitness_s = max;
    				}

    			}
    		}
    		NodeList maxSpecies = maxGenNode.getElementsByTagName("specie");
    		String chromID = null;
    		for(int index = 0; index< maxSpecies.getLength() && chromID == null; index++) {
    			Element speciesNode =  (Element) maxSpecies.item(index);
    			NodeList chromosomes = speciesNode.getElementsByTagName("chromosome");
    			for(int chromIndex = 0; chromIndex < chromosomes.getLength(); chromIndex++) {
    				Element chrom = (Element) chromosomes.item(chromIndex);
    				if(chrom.getAttribute("fitness").equals(maxFitness_s)) {
    					chromID = chrom.getAttribute("id");
    					break;
    				}
    			}
    		}
    		System.out.println("totalChamp:" + chromID);
    		return chromID;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
    

    public static String[] findChampChromosomes() {
    	try {
    		File fXmlFile = new File("nevt/fitness/fitness.xml");
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		Document doc = dBuilder.parse(fXmlFile);
    				
    		//optional, but recommended
    		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
    		doc.getDocumentElement().normalize();
    		
    		
    		ArrayList<String> champions = new ArrayList();
    		NodeList nodeList =	doc.getElementsByTagName("generation");
    		for(int i = 0; i < nodeList.getLength(); i++) {
    			Node nNode = nodeList.item(i);
    			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
    				Element generation = (Element) nNode;
    				Element fitness = (Element) generation.getElementsByTagName("fitness").item(0);
    				String max = ((Element)fitness.getElementsByTagName("max").item(0)).getTextContent();
    				double maxVal = Integer.parseInt(max);

    				NodeList speciesList = generation.getElementsByTagName("specie");
    				String chromID = null;
    				for(int index = 0; index< speciesList.getLength() && chromID == null; index++) {
    	    			Element speciesNode =  (Element) speciesList.item(index);
    	    			NodeList chromosomes = speciesNode.getElementsByTagName("chromosome");
    	    			for(int chromIndex = 0; chromIndex < chromosomes.getLength(); chromIndex++) {
    	    				Element chrom = (Element) chromosomes.item(chromIndex);
    	    				if(chrom.getAttribute("fitness").equals(max)) {
    	    					chromID = chrom.getAttribute("id");
    	    					break;
    	    				}
    	    			}
    	    		}
    				if(champions.contains(chromID) == false) {
        				champions.add(chromID);
    				}
    			}
    		}
    		return champions.toArray(new String[champions.size()]);
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
    
    
    public static void testEvolve(String propertiesFile) {
    	Logger logger = Logger.getLogger( Evolver.class );
    	try {
    		Properties props = new Properties( propertiesFile);
    		Evolver evolver = new Evolver();
    		evolver.init( props );
    		evolver.run();
    	}
    	catch ( Throwable th ) {
    		logger.error( "", th );
    		th.printStackTrace();
    	}
    }
    
    // Janky Java
    public static void testCmd() {
        try {
            // create new bat file, run bat file
            File batFile = new File("test.bat");
            PrintWriter pw = new PrintWriter(batFile);
            pw.println("evolve.bat maze.properties");
            pw.close();
            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("test.bat");
            
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(pr.getInputStream()));

            BufferedReader stdError = new BufferedReader(new InputStreamReader(pr.getErrorStream()));

            String s = null;
            int count = 0;
            String championData = "";
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                if(s.contains("champ:")){
                    championData = s;
                }
            }
            batFile.deleteOnExit();
            System.out.println("CHAMPIONDATA:\n" + championData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void testScoreAll() {
        int testSize = 100;
        // MazeGenerator.Score.scoreAllCoordFormat(100);
        MazeGenerator.Score.scoreAllTableFormat(100);
    }

    public static void testScore() {
        Queue<char[][]> mazes = MazeGenerator.Load.multiple(200);
        char[][] maze0 = mazes.poll();
        Character[][] maze0Character = MazeGenerator.Util.charToCharacter(maze0);
        Coordinate2d start = MazeGenerator.Util.findFirst(maze0Character, 'C'), end = MazeGenerator.Util.findLast(maze0Character, 'C');
        BFS2d path = new BFS2d(start, end, maze0);
        Coordinate2d[] pruned = path.backtrackMaze();
        double distance_to_end = Coordinate2d.realDistance(start, end);
        List<String> inputList = new ArrayList(), outputList = new ArrayList();
        for (int i = 0; i < pruned.length; i++) {
            List<Character> surr = pruned[i].getNeighborValues(maze0Character);
            List<Integer> surr_int = new ArrayList();
            for (Character c : surr) {
                surr_int.add((c == 'C' ? 1 : 0));
            }
            double distance_normal = Coordinate2d.realDistance(pruned[i], end) / distance_to_end;
            double numDigits = Math.pow(10.0, (double) (int) Math.log(distance_to_end));
            distance_normal = (int) (distance_normal * numDigits) / numDigits;
            String input = "";
            input += distance_normal + ";";
            for (Integer tempInt : surr_int) {
                input += tempInt + ";";
            }
            System.out.print(input + "\t");
            inputList.add(input);

            String expectedOutput = "";
            int direction = -1;
            if (i < pruned.length - 1) {
                Coordinate2d nextNode = pruned[i + 1];
                for (int j = 0; j < pruned[i].getNeighbors().length; j++) {
                    if (nextNode.equals(pruned[i].getNeighbors()[j])) {
                        direction = j;
                        break;
                    }
                }
            } else {
                direction = 4;
            }
            final String[] direction_map = new String[]{"0;1;0;0", "1;0;0;0", "0;0;1;0", "0;0;0;1", "0;0;0;0"};
            expectedOutput = direction_map[direction];
            System.out.print("output:" + expectedOutput + "\n");
            outputList.add(expectedOutput);

        }
        String[] inputArr = inputList.toArray(new String[inputList.size()]);
        String[] outputArr = outputList.toArray(new String[outputList.size()]);
        try {
            files.saveFile(inputArr, "../../data/inputs.txt");
            files.saveFile(outputArr, "../../data/outputs.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void testGenetic() {
        Queue<char[][]> mazes = MazeGenerator.Load.multiple(10);
        char[][] maze0 = mazes.poll();
        Character[][] maze0Character = MazeGenerator.Util.charToCharacter(maze0);
        Coordinate2d start = MazeGenerator.Util.findFirst(maze0Character, 'C'), end = MazeGenerator.Util.findLast(maze0Character, 'C');
        System.out.printf("Start:%s\tEnd:%s\n", start, end);
        Genetic2d gPath = new Genetic2d(start, end, maze0);
        gPath.isActive = true;
        gPath.run(100);
        Direction[] dir = gPath.breeder.findMostFitIndividual();

        Coordinate2d prev = start;
        Coordinate2d curr = start;
        double penalties = 0;
        for (Path2d.Direction d : dir) {
            prev = curr;
            curr = nextNode(curr, d);
            if (gPath.isWall(curr)) {
                curr = prev;
                penalties++;
            }
        }
        System.out.println("Last node of a maze run:" + curr);
        System.out.println("isWall prev:" + gPath.isWall(prev));
        System.out.println("isWall curr:" + gPath.isWall(curr));
        System.out.println("fitness:" + gPath.breeder.getFitness(gPath.breeder.findMostFitIndividual()));
        MazeGenerator.printMaze(maze0);
    }

}
